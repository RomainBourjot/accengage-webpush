<?php

/*
Plugin Name: Accengage Webpush Demo
Plugin URI:  https://developer.wordpress.org/plugins/the-basics/
Description: Accengage Webpush Demo.
Version:     3.1.2
Author:      Romain Bourjot
License:     All rights reserved
*/

//Security check
defined('ABSPATH') or die('[...]');

global $acc_i18n_;

$acc_i18n_ = array(
	'fr'=>array(
		'webpush'=>'Recevoir nos Pushs Web : ',
		'tel'=>'Tél : +33.1.44.56.87.16'
	),
	'en'=>array(
		'webpush'=>'Receive our Web Push Notifications: ',
		'tel'=>'Tel: +44.203.290.5248'
	),
	'es'=>array(
		'webpush'=>'Recibir nuestros Web Push: ',
		'tel'=>'Tel: +33.1.44.56.87.16'
	),
	'de'=>array(
		'webpush'=>'Web Push Nachrichten aktivieren: ',
		'tel'=>'Tel: +33.1.44.56.87.16'
	),
	'it'=>array(
		'webpush'=>'Ricevi le nostre Notifiche Push: ',
		'tel'=>'Tel: +33.1.44.56.87.16'
	)
);

function acc_i18n($str) {
	global $acc_i18n_;
	if (array_key_exists(acc_get_lang(), $acc_i18n_)) {
		if (array_key_exists($str, $acc_i18n_[acc_get_lang()])) {
			return $acc_i18n_[acc_get_lang()][$str];
		}
	}

	return "";
}





function acc_webpush_assets() {
	if (is_admin()) { return; }


	wp_register_script('acc-webpush-js', plugins_url('acc-webpush-3.1.3.js', __FILE__) , array('jquery'), '3.1.3', true);
	wp_register_style('acc-webpush-css', plugins_url('acc-webpush-3.1.3.css', __FILE__), array(), '3.1.3');
	wp_register_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), '3.1.3');
}





function acc_webpush_shortcode_defaults($attrs) {
	if (is_admin()) { return ""; }

	wp_enqueue_script('acc-webpush-js');
	wp_enqueue_style('acc-webpush-css');
	wp_enqueue_style('font-awesome');

	if (!current_user_can('administrator')) {
		if (array_key_exists('public', $attrs)) {
			return $attrs['public'];
		}
	}

	include_once(plugin_dir_path(__FILE__).'Mobile_Detect.php');

	$mb_detect = new Mobile_Detect;


	if ($mb_detect->isMobile()) {
		if (array_key_exists('mobile', $attrs)) {
			return $attrs['mobile'];
		}
	}

	return null;
}





function acc_webpush_mobile_button($attrs) {
	$defaults = acc_webpush_shortcode_defaults($attrs);

	if ($defaults !== null) { return $default; }

	include_once(plugin_dir_path(__FILE__).'Mobile_Detect.php');

	$mb_detect = new Mobile_Detect;
	$mobile = $mb_detect->isMobile() == true ? 'true' : 'false';
	return PHP_EOL.'<script>window.ACC_ACTIVATE_MOBILE_BUTTON = '.$mobile.';</script>'.PHP_EOL;
}





function acc_webpush_button($attrs) {
	$defaults = acc_webpush_shortcode_defaults($attrs);

	if ($defaults !== null) { return $defaults; }

	return PHP_EOL.'<script>window.ACC_ACTIVATE_FLOATING_BUTTON=true;</script>'.PHP_EOL;
}






function acc_webpush_switch($attrs) {
	$defaults = acc_webpush_shortcode_defaults($attrs);

	if ($defaults !== null) { return $defaults; }

	return '';
}






function acc_webpush_header_switch($attrs) {
	$defaults = acc_webpush_shortcode_defaults($attrs);

	if ($defaults !== null) { return $defaults; }

	return '<script>window.ACC_ACTIVATE_SWITCH = true; window.ACC_SWITCH_LABEL = "'.acc_i18n('webpush').'"; window.ACC_SWITCH_TEL = "'.acc_i18n('tel').'";</script>';
}






function acc_get_lang() {
	$lang = 'en';
	$langs = array('en', 'fr', 'es', 'de', 'it');

	if (defined('ICL_LANGUAGE_CODE')) {
		$lang = ICL_LANGUAGE_CODE;

		if (!in_array(ICL_LANGUAGE_CODE, $langs)) {
			$lang = 'en';
		}
	}

	return $lang;
}





// OK
function acc_webpush_lang() {
	echo '<script>window.acc_lang = "'.acc_get_lang().'";</script>';
}





add_action('wp_enqueue_scripts', 'acc_webpush_assets');
add_action('wp_head', 'acc_webpush_lang'); // OK

add_shortcode('acc_webpush_switch', 'acc_webpush_switch');
add_shortcode('acc_webpush_header_switch', 'acc_webpush_header_switch');
add_shortcode('acc_webpush_button', 'acc_webpush_button');
add_shortcode('acc_webpush_mobile_button', 'acc_webpush_mobile_button');
