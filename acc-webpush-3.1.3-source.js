(function() {

'use strict';

var acc_lang = window.acc_lang||'en';
window.ACC3 = window.ACC3 || [];
var acc_current_state = 'not_compliant';
var acc_modal_opened = false;
var $ = $||jQuery;
var acc_webpush_elements = [];




// Loading SDK
(function(l,o,a,d,i,n,g,w,e,b){
  g='AccengageWebSDKObject';w='script';l[g]=l[g]||{};l[g][n]=d;
  l[d]=l[d]||[];l[d].p={'date':1*new Date(),'window':l,'document':o,'params':a};
  e=o.createElement(w);b=o.getElementsByTagName(w)[0];e.async=1;
  e.src='https://'+n+i+'/init.js';b.parentNode.insertBefore(e,b);
})(window,document,{},'ACC3','/pushweb/assets','webpush-by.accengage.net');





function set_cookie_fn(name, value) {
	if (!sessionStorage) { return null; }
	try { localStorage.setItem(String(name),String(value)); } catch (e) { return; }
}

function get_cookie_fn(name) {
	if (!sessionStorage) { return null; }

	return localStorage.getItem(String(name));
}





var compliant_navs = 	'<div style="width: 100%; display: flex; flex-wrap: nowrap;align-items: stretch; padding-top: 1em">'+
						'<div style="flex: 1; text-align: center"><a target="_blank" id="modal_chrome_link" href="https://www.google.com/chrome/browser/desktop/index.html">Google Chrome<br/><div style="font-size: 0.66em;">version 42+<br/></div>'+
						'<img style="padding-top: 1em" src="http://www.accengage.com/wp-content/uploads/2016/11/logo-chrome.png" width="64" height="64" alt="Google Chrome" /></a></div>'+
						'<div style="flex: 1; text-align: center"><a target="_blank" id="modal_ff_link" href="https://www.mozilla.org/firefox">Mozilla Firefox<br/><div style="font-size: 0.66em;">version 44+<br/></div>'+
						'<img style="padding-top: 1em" src="http://www.accengage.com/wp-content/uploads/2016/11/firefox-128.png" width="64" height="64" alt="Mozilla Firefox" /></a></div>'+
						'</div>';

var _i18n_str = {
	'fr': {
		'optin_state_text':'Push Web : ',
		'soft_output_state_text':'Push Web :',
		'hard_output_state_text':'Push Web :',
		'optout_link': 'http://www.accengage.com/fr/comment-reactiver-le-push-web-sur-mon-navigateur/',
		'notcompliant_link':'./notcompliant.html',
		'activate_web_push':'Activer les notifications !',
		'bubble_text':'Activer le Push Web !',
		'not_compliant_modal_content':  '<div style="text-align: center; margin-top: 5px"><h1>Oups !</h1>Vous ne faites pas partie des 68% d\'utilisateurs<br/>qui utilisent un navigateur compatible avec le Push Web.</div>'+compliant_navs+'<div style="text-align: center; font-size: 0.66em; margin-top: 20px">Néanmoins, suite à la standardisation du Push Web par le W3C et le WHATWG,<br/>votre navigateur devrait bientôt être supporté.</div>',
		'mobile_button_optin': 'Push Web Activé',
		'mobile_button_unknown': 'Activer le Push Web'

	},
	'en': {
		'optin_state_text':'Web Push:',
		'soft_output_state_text':'Web Push:',
		'hard_output_state_text':'Web Push:',
		'optout_link': 'http://www.accengage.com/how-do-i-reactivate-web-push-notifications-on-my-browser/',
		'notcompliant_link':'./frnotcompliant.html',
		'activate_web_push':'Activate Notifications',
		'bubble_text':'Activate Web Push Notifications!',
		'not_compliant_modal_content':  '<div style="text-align: center; margin-top: 5px"><h1>Oups!</h1>You don\'t belong to the 68% of users<br/>who use a browser compliant with Web Push Notifications.</div>'+compliant_navs+'<div style="text-align: center; font-size: 0.66em; margin-top: 20px">However, following the Web Push standardization by the W3C and the WHATWG,<br/>your web browser should soon be supported.</div>',
		'mobile_button_optin': 'Web Push Notifications Activated',
		'mobile_button_unknown': 'Activate Web Push Notifications'
	},
	'es': {
		'optin_state_text':'Web Push:',
		'soft_output_state_text':'Web Push:',
		'hard_output_state_text':'Web Push:',
		'optout_link': 'http://www.accengage.com/es/como-vuelvo-a-activar-las-notificaciones-web-push-en-mi-navegador/',
		'notcompliant_link':'./frnotcompliant.html',
		'activate_web_push':'Activate Notifications',
		'bubble_text':'¡Activar las Notificaciones Web Push!',
		'not_compliant_modal_content':  '<div style="text-align: center; margin-top: 5px"><h1>Oups!</h1>Usted no pertenece al 68% de los usuarios que utilizan un navegador compatible con notificaciones Web Push..</div>'+compliant_navs+'<div style="text-align: center; font-size: 0.66em; margin-top: 20px">Sin embargo, tras la normalización de Web Push por el W3C y el WHATWG,<br/>su navegador web deberá estar pronto disponible.</div>',
		'mobile_button_optin': 'Web Push Activado',
		'mobile_button_unknown': 'Activar Web Push'
	},
	'it': {
		'optin_state_text':'Web Push:',
		'soft_output_state_text':'Web Push:',
		'hard_output_state_text':'Web Push:',
		'optout_link': 'http://www.accengage.com/it/come-reattivare-le-notifiche-push-sul-mio-navigatore/',
		'notcompliant_link':'./frnotcompliant.html',
		'activate_web_push':'Activate Notifications',
		'bubble_text':'Attiva le Notifiche Web Push!',
		'not_compliant_modal_content':  '<div style="text-align: center; margin-top: 5px"><h1>Oups!</h1>Non fai parte del 68% di utenti che usano un navigatore compatibile con le Web Push.</div>'+compliant_navs+'<div style="text-align: center; font-size: 0.66em; margin-top: 20px">Nientemeno, in seguito alla standardizzazione del Push Web, operata da W3C e il WHATWG,<br/>il tuo navigatore sarà supportato a breve. </div>',
		'mobile_button_optin': 'Push Web Attive',
		'mobile_button_unknown': 'Attiva le Push Web'
	},
	'de': {
		'optin_state_text':'Web Push:',
		'soft_output_state_text':'Web Push:',
		'hard_output_state_text':'Web Push:',
		'optout_link': 'http://www.accengage.com/de/wie-reaktiviere-ich-web-push-benachrichtigungen-auf-meinem-browser/',
		'notcompliant_link':'./frnotcompliant.html',
		'activate_web_push':'Activate Notifications',
		'bubble_text':'Aktivieren Sie Web Push-Benachrichtigungen',
		'not_compliant_modal_content':  '<div style="text-align: center; margin-top: 5px"><h1>Ups!</h1>Sie gehören nicht zu den 68% der Nutzer, welche einen kompatiblen Browser für Web Push-Benachrichtigungen verwenden.</div>'+compliant_navs+'<div style="text-align: center; font-size: 0.66em; margin-top: 20px">Nach der Web Push Standardisierung der W3C und WHATWG<br/>sollte Ihr Bowser jedoch bald unterstützt werden.</div>',
		'mobile_button_optin': 'Web Push aktiviert',
		'mobile_button_unknown': 'Aktivieren Sie Web Push-Nachrichten'
	}
};

function _i18n(str) {
  var to_ret = false;

	if (!_i18n_str[acc_lang]) {
    to_ret = _i18n_str.en[str] || ' ';
	} else if (!_i18n_str[acc_lang][str]) {
		to_ret = ' ';
	}

	return to_ret || _i18n_str[acc_lang][str];
}





window.acc_webpush_show_not_compliant_page = function() {
	window.open(_i18n('notcompliant_link'), 'not_compliant_wd', 'scrollbars=yes,height=640px,width=780px,location=no,menubar=no,status=no,titlebar=no,toolbar=no');
};





(function(){





  /**
   * La flèche qui s'afffiche à côté de la notification.
   *
   * @constructor
   */
  function ACCWebPushArrow() {
    this.$wrapper = $('<div/>');
    this.state = undefined;
  }

  /**
   * Getter pour le <div> principal du switch.
   *
   * @public
   */
  ACCWebPushArrow.prototype.get_wrapper = function() { return this.$wrapper; };

  /**
   * Passe la flèche à l'état optin.
   *
   * @public
   */
  ACCWebPushArrow.prototype.optin = function() {
    if (this.state !== 'unknown') { return; }

    var Y = (32-screen.availHeight+window.outerHeight+window.screenY);
    var X = (400-screen.availWidth+window.outerWidth+window.screenX);

    if (X < 0 || Y < 0) { return; }
    if (X > $(window).width() || Y > $(window).height()) { return; }
    if (navigator.appVersion.indexOf("Win")===-1) { return; }

    $(document.body).on('click', '.acc-modal-background', function(event){event.stopPropagation(); return false;});
    var bg = $('<div/>',{'class':'acc-modal-background'});
    var arrow = $('<div/>',{'class':'acc-modal-arrow'});

    arrow.css({ bottom:Y+'px', right:X+'px' });

    bg.append(arrow);

    $('body').append(bg);

    bg.fadeIn();
    window.setTimeout(function() { $('.acc-modal-background').fadeOut('slow',function(){$('.acc-modal-background').remove();}); },5000);
  };

  /**
   * Passe la flèche à l'état optout.
   * VOID
   *
   * @public
   */
  ACCWebPushArrow.prototype.optout = function() { return; };

  /**
   * Passe la flèche à l'état unknown (pas de préférence / soft optout)
   * VOID
   *
   * @public
   */
  ACCWebPushArrow.prototype.unknown = function() { this.state = 'unknown'; };





  /**
   * Le switch dans le header
   *
   * @constructor
   */
  function ACCWebPushSwitch() {
    this.$wrapper = $('<div class="acc-webpush__switch--wrapper" />');
    this.$wrapper_in = $('<div>');
    this.$label = $('<div class="acc-webpush__switch--label" />');
    var $container = $('<div class="acc-webpush__switch--container" />');
    var $toggle = $('<div class="acc-webpush__switch--toggle" />');
    this.$link = $('<a class="acc-webpush__switch--link" />');
    this.$input = $('<input type="checkbox" class="acc-webpush__switch--check" />');
    var $_switch = $('<b class="acc-webpush__switch--b acc-webpush__switch--button" />');
    var track = $('<b class="acc-webpush__switch--b acc-webpush__switch--track" />');

    this.$tel = $('<div class="acc-webpush__switch--tel" />');

    $toggle.append(this.$link);
    this.$link.append(this.$input);
    this.$link.append($_switch);
    this.$link.append(track);

    $container.append($toggle);
    this.$wrapper_in.append(this.$label);
    this.$wrapper_in.append($container);
    this.$wrapper_in.append(this.$tel);
    this.$wrapper.append(this.$wrapper_in);
  }

  /**
   * Défini le numéro de téléphone et le texte à gauche du switch.
   *
   */
  ACCWebPushSwitch.prototype.set_text = function(label, tel) {
    this.$label.text(label);
    this.$tel.text(tel);
  };

  /**
   * Getter pour le <div> principal du switch.
   *
   * @public
   */
	ACCWebPushSwitch.prototype.get_wrapper = function() { return this.$wrapper; };

  /**
   * Défini l'état du switch :
   * - optin
   * - optout
   * - unknown (soft optout / pas de préférence)
   *
   * @private
   */
	ACCWebPushSwitch.prototype._set_state = function(state) {
    this.state = state;
    this.$input.attr('data-acc-wp-state', state);
  };

  /**
   * Détache l'évenement launchLandingOnClick.
   *
   * @private
   */
  ACCWebPushSwitch.prototype._remove_event = function() {
    var cloned = this.$wrapper_in.clone();
    this.$wrapper_in.remove();
    this.$wrapper.append(cloned);
    this.$wrapper_in = cloned;
    this.$link = this.$wrapper_in.find('.acc-webpush__switch--link');
    this.$input = this.$wrapper_in.find('.acc-webpush__switch--check');
    this.previous_state = 'unknown';
  };

  /**
   * L'utilisateur est optin sys, mais préférence optout
   *
   * @private
   */
  ACCWebPushSwitch.prototype._fake_optout = function() {
    if (this.previous_state !== 'optin') {
      this._remove_event();
    }
    this.$link.off();
    this._set_state('optout');

    var that = this;
    this.$link.click(function() {
      store_fake_optin_fn(true);
      that.optin.call(that);
    });
  };

  /**
   * Passe le switch à l'état optin
   *
   * @public
   */
  ACCWebPushSwitch.prototype.optin = function() {
    if (this.previous_state !== 'optin') {
      this._remove_event();
    }
    this.previous_state = 'optin';
    this.$link.off();

    if (get_fake_optin_fn()) {
      this._set_state('optin');
    } else {
      return this._fake_optout();
    }

    var that = this;
    this.$link.click(function() {
      store_fake_optin_fn(false);
      that._fake_optout.call(that);
    });
  };

  /**
   * Passe le switch à l'état optout
   *
   * @public
   */
  ACCWebPushSwitch.prototype.optout = function() {

    this.$link.off();
    this._remove_event();
    this._set_state('optout');

    this.$link.click(function() {
      window.open(_i18n('optout_link'), 'optout_wd', 'height=640px,width=780px,scrollbars=yes,location=no,menubar=no,status=no,titlebar=no,toolbar=no');
    });
  };

  /**
   * Passe le switch à l'état unknown (pas de préférence / soft optout)
   *
   * @public
   */
  ACCWebPushSwitch.prototype.unknown = function() {
    this._set_state('unknown');

		window.ACC3.push([
			'push:launchLandingOnClick',
			[ '.acc-webpush__switch--link' ],
			{ }
		]);

		window.ACC3.push([
		    "push:addCustomListeners",
		    {
		        "landingFeedback:optin": function() {
              store_fake_optin_fn(true);
							set_global_state('optin');

		        },
		        "landingFeedback:hardOptout": function() {
							set_global_state('optout');

		        },
            "landingFeedback:softOptout": function() { return; }
		    }
		]);
  };










  /**
   * Bouton flottant en bas à gauche.
   *
   * @constructor
   */
  function ACCFloatingWebPushButton() {
    this.$wrapper = $('<div class="acc-webpush__floating-button--wrapper" />');
    this.$link = $('<a class="acc-webpush__floating-button--link" />');
    this.$button = $('<div class="acc-webpush__floating-button--button sonar-stroke" />');
    this.$button.append($('<i/>',{'class':'fa fa-bell acc-white', 'aria-hidden':'true'}));

    this.$bubble = $('<div class="acc-webpush__floating-button--bubble" />');
    this.$bubble.text(_i18n('bubble_text'));

    this.$link.append(this.$bubble);
    this.$link.append(this.$button);
    this.$wrapper.append(this.$link);

    this.blink_interval = null;
    this._bubble__is_hover = false;
    this._bubble_hide_timeout = 0;
  }

  /**
   * Arrete l'animation sonar.
   *
   * @private
   */
  ACCFloatingWebPushButton.prototype._unblink = function() {
      if (this.blink_interval !== null) {
        window.clearInterval(this.blink_interval);
        this.blink_interval = null;
      }
  };

  /**
   * Animation sonar. Doit être appelée dans un setInterval
   *
   * @private
   */
  ACCFloatingWebPushButton.prototype._blink = function() {
    var that = this;

    that.$button.addClass('sonar');
    window.setTimeout(function() { that.$button.removeClass('sonar'); }, 1000);
  };

  /**
   * Affiche la bulle au dessus du bouton.
   *
   * @private
   */
  ACCFloatingWebPushButton.prototype._show_bubble = function() {
    if (this.$bubble.is(':hidden')) {
      var that = this;
      this.$bubble.fadeIn('slow');
      if (!this._bubble_is_hover) {
        this._bubble_hide_timeout = window.setTimeout(function() { that._hide_bubble(); }, 5000);
      }
    }
  };

  /**
   * Cache la bulle au dessus du bouton
   *
   * @private
   */
  ACCFloatingWebPushButton.prototype._hide_bubble = function() {
    this.$bubble.fadeOut('slow');
  };

  /**
   * Getter pour le <div> principal du bouton.
   *
   * @public
   */
  ACCFloatingWebPushButton.prototype.get_wrapper = function() { return this.$wrapper; };

  /**
   * Défini l'état du bouton :
   * - optin
   * - optout
   * - unknown (soft optout / pas de préférence)
   *
   * @private
   */
  ACCFloatingWebPushButton.prototype._set_state = function(state) { this.state = state; this.$wrapper.attr('data-acc-wp-state', state); };

  /**
   * Passe le bouton à l'état optin
   *
   * @public
   */
  ACCFloatingWebPushButton.prototype.optin = function() { this._unblink(); this._set_state('optin'); };

  /**
   * Passe le bouton à l'état optout
   *
   * @public
   */
  ACCFloatingWebPushButton.prototype.optout = function() { this._unblink(); this._set_state('optout'); };

  /**
   * Passe le bouton à l'état unknown (pas de préférence / soft optout)
   *
   * @public
   */
  ACCFloatingWebPushButton.prototype.unknown = function() {
    var that = this;

    this.$bubble.hide();

    this.$link.mouseover(function() {
      window.clearTimeout(that._bubble_hide_timeout);
      that._bubble_is_hover = true;
      that._show_bubble();
    });

    this.$link.mouseout(function() {
      that._bubble_is_hover = false;
      that._bubble_hide_timeout = window.setTimeout(function() {
        that._hide_bubble();
      }, 5000);
    });

    this._set_state('unknown');

    window.setTimeout(function() { that._show_bubble(); }, 5000);

    window.ACC3.push([
			'push:launchLandingOnClick',
			[ '.acc-webpush__floating-button--link' ],
			{ }
		]);

		window.ACC3.push([
		    "push:addCustomListeners",
		    {
		        "landingFeedback:optin": function() {
							set_global_state('optin');

		        },
		        "landingFeedback:hardOptout": function() {
							set_global_state('optout');

		        },
            "landingFeedback:softOptout": function() { return; }
		    }
		]);

    this.blink_interval = window.setInterval(function() { that._blink(); }, 3000);
  };










  /**
   * Boutont d'optinisation sur mobile.
   *
   * @public
   */
	function ACCMobileWebPushButton() {
		this.$wrapper = $('<div class="acc-webpush__mobile-button--wrapper" />');
		this.$button = $('<a class="acc-webpush__mobile-button" href="" />');

		this.$wrapper.append(this.$button);

		this.$button.click(function() { return false; });
	}

  /**
   * Getter pour le <div> principal du bouton.
   *
   * @public
   */
	ACCMobileWebPushButton.prototype.get_wrapper = function() { return this.$wrapper; };

  /**
   * Défini l'état du bouton :
   * - optin
   * - optout
   * - unknown (soft optout / pas de préférence)
   *
   * @private
   */
	ACCMobileWebPushButton.prototype._set_state = function(state) { this.state = state; this.$button.attr('data-acc-wp-state', state); };

  /**
   * Détache l'évenement launchLandingOnClick.
   *
   * @private
   */
  ACCMobileWebPushButton.prototype._remove_event = function() {
    var cloned = this.$button.clone();
    this.$button.remove();
    this.$wrapper.append(cloned);
    this.$button = cloned;
    this._previous_state = 'unknown';
  };

  /**
   * L'utilisateur est optin sys, mais préférence optout
   *
   * @private
   */
  ACCMobileWebPushButton.prototype._fake_optout = function() {
    if (this.previous_state !== 'optin') { this._remove_event(); }

    this.$button.off();
    this._set_state('unknown');
    this.$button.text(_i18n('mobile_button_unknown'));

    var that = this;
    this.$button.click(function() {
      store_fake_optin_fn(true);
      that.optin.call(that);

      return false;
    });
  };

  /**
   * Passe le bouton à l'état optin
   *
   * @public
   */
	ACCMobileWebPushButton.prototype.optin = function() {
		this.$button.text(_i18n('mobile_button_optin'));
		this._set_state('optin');


    if (this.previous_state !== 'optin') {
      this._remove_event();
    }

    if (this.$button.is(':hidden')) {
      this.$wrapper.fadeIn('slow');
    }

    this.previous_state = 'optin';
    this.$button.off();

    if (get_fake_optin_fn()) {
      this._set_state('optin');
    } else {
      return this._fake_optout();
    }

    var that = this;
    this.$button.click(function() {
      store_fake_optin_fn(false);
      that._fake_optout.call(that);

      return false;
    });
	};

  /**
   * Passe le bouton à l'état optout
   *
   * @public
   */
	ACCMobileWebPushButton.prototype.optout = function() {
    this._set_state('optout');
    this.$wrapper.remove();
    $('.header-v2').addClass('acc-webpush__mobile-button--no_border');
	};

  /**
   * Passe le bouton à l'état unknown (pas de préférence / soft optout)
   *
   * @public
   */
	ACCMobileWebPushButton.prototype.unknown = function() {
		this.$button.text(_i18n('mobile_button_unknown'));

    this.$wrapper.fadeIn('slow');

		this._set_state('unknown');

		window.ACC3.push([
			'push:launchLandingOnClick',
			[ '.acc-webpush__mobile-button' ],
			{ }
		]);

		window.ACC3.push([
		    "push:addCustomListeners",
		    {
		        "landingFeedback:optin": function() {
              store_fake_optin_fn(true);
							set_global_state('optin');

		        },
		        "landingFeedback:hardOptout": function() {
							set_global_state('optout');

		        },
            "landingFeedback:softOptout": function() { return; }
		    }
		]);
	};










	function un_blur() {
		var filterVal = 'blur(0px)';

		$.each($(document.body).children(),function(k,it){
			$(it).css('filter',filterVal)
		  		.css('webkitFilter',filterVal)
		  		.css('mozFilter',filterVal)
		  		.css('oFilter',filterVal)
		  		.css('msFilter',filterVal);
		});
	}






	window.acc_webpush_show_not_compliant_page = function() {
		if ($('#not-compliant-modal').length) { return; }

		var modal = $('<div/>', {'id': 'not-compliant-modal'});
		var close_btn = $('<span/>', {'id':'not-compliant-modal-close-btn'});


		var modal_content = $('<div>',{'id':'not-compliant-modal-content'});
		var modal_content_p = $('<p/>');

		close_btn.html('<i class="fa fa-times" aria-hidden="true"></i>');
		modal_content_p.html(_i18n('not_compliant_modal_content'));

		modal_content.append(close_btn);
		modal_content.append(modal_content_p);
		modal.append(modal_content);

		$('body').append(modal);

		modal.fadeIn('fast');

		var filterVal = 'blur(2px)';

		$.each($(document.body).children(),function(k,it){
			if ($(it).attr('id') !== "not-compliant-modal") {
				$(it).css('filter',filterVal)
			  		.css('webkitFilter',filterVal)
			  		.css('mozFilter',filterVal)
			  		.css('oFilter',filterVal)
			  		.css('msFilter',filterVal);
			}
		});

		$(document.body).on('click', '#not-compliant-modal', function(){un_blur();modal.fadeOut('fast',function(){modal.remove();});});
		$(document.body).on('click', '#not-compliant-modal-content', function(event){event.stopPropagation(); return false;});
		if (!acc_modal_opened) {
			$(document.body).on('click', '#modal_chrome_link', function(){window.open('https://www.google.com/chrome/browser/desktop/index.html', 'Google Chrome');});
			$(document.body).on('click', '#modal_ff_link', function(){window.open('https://www.mozilla.org/firefox', 'Mozilla Firefox');});
			acc_modal_opened = true;
		}
		$(document.body).on('click', '#not-compliant-modal-close-btn', function(){un_blur();modal.fadeOut('fast',function(){modal.remove();});});


	};










	/**
   * Sauvegarde l'optin préférence en local et push sur le serveur.
   *
   */
	function store_fake_optin_fn(optin) {
	    optin = optin ? 'optin' : 'optout';

	    set_cookie_fn('acc-webpush-local-optin', optin);

	    window.ACC3.push([
		    "core:updateDeviceInfo",
		    { prefoptin: optin }
			]);

	}

	/**
   * Récupére l'optin préférence en local.
   *
   */
	function get_fake_optin_fn() {
		var val = get_cookie_fn('acc-webpush-local-optin');

        if (val === 'optin') {
        	return true;
        } else if (val === 'optout') {
        	return false;
        }

	    return null;
	}










	/**
   * Vérifie que le navigateur est compatible.
   * Retourne une resolved promise si oui, rejected sinon.
	 *
	 * @return {Promise}
	 */
	function check_compliance() {
		var defer = $.Deferred();

		window.ACC3.push([
			"core:isCompliantWithPushPlugin",
    	null,
			{
				onSuccess: function() { return defer.resolve(); },
				onError: function() { return defer.reject(); }
			}
		]);

		return defer.promise();
	}










	/**
	 * Vérifie le statut de l'utilisateur.
   * Retourne une resolved Promise avec le statut en argument : 'optin', 'optout', 'unknown'.
   * Retourne uen rejected Promise en cas d'erreur.
	 *
	 * @return {Promise}
	 */

	function is_optin_sys_fn() {
		var defer = $.Deferred();

		window.ACC3.push([
			'core:isOptin',
			null,
			{
				onSuccess: function() {
					defer.resolve('optin'); // user is optin
				},
				// BUG err.details.notificationPermission fonctionne pas
				'onError': function() {
					var testa = $('<a id="fooa" />');

					$('body').append(testa);

					window.ACC3.push([
						'push:launchLandingOnClick',
						[ '#fooa' ],
						{
							'onSuccess':function(){
								testa.remove();
								 return defer.resolve('unknown');
							},
							'onError':function(err) {
								testa.remove();

								if (err.toLowerCase() === 'native push permission is denied') {
									return defer.resolve('optout');
								}

								return defer.reject(err);
							}
						}
					]);
				}
			}]);

			return defer.promise();
	}










  /**
   * Creer les éléments du web push.
   *
   * @returns {Promise}
   */
  function create_elements() {
    var defer = $.Deferred();
    var acc_webpush_mobile_button = new ACCMobileWebPushButton();
    var acc_webpush_floating_button = new ACCFloatingWebPushButton();
    var acc_webpush_switch = new ACCWebPushSwitch();
    var acc_webpush_arrow = new ACCWebPushArrow();

    var done = { wp_mobile_button:false, wp_switch:false };
    var done_cb = function() {
      if ((done.wp_mobile_button||!window.ACC_ACTIVATE_MOBILE_BUTTON) && (done.wp_switch||!window.ACC_ACTIVATE_SWITCH)) {
        window.setTimeout(function() { defer.resolve(); }, 0);
      }
    };

    acc_webpush_elements = [ ];





		if (window.ACC_ACTIVATE_MOBILE_BUTTON) {
      var $mbutton_wrapper = acc_webpush_mobile_button.get_wrapper();
      $mbutton_wrapper.hide();
      $('.header-wrapper').prepend($mbutton_wrapper);
      $('.header-v2').addClass('acc-webpush__mobile-button--no_border');
      acc_webpush_elements.push(acc_webpush_mobile_button);
      window.setTimeout(function() { done.wp_mobile_button = true; done_cb(); }, 0);
    }





    if (window.ACC_ACTIVATE_FLOATING_BUTTON) {
      $('body').append(acc_webpush_floating_button.get_wrapper());
      acc_webpush_elements.push(acc_webpush_floating_button);
    }






    if (window.ACC_ACTIVATE_SWITCH) {

      $('.header-info').fadeOut('slow', function() {
        $('.header-info').empty();
        acc_webpush_switch.set_text(window.ACC_SWITCH_LABEL, window.ACC_SWITCH_TEL);
        $('.header-info').append(acc_webpush_switch.get_wrapper());
        acc_webpush_elements.push(acc_webpush_switch);

        $('.header-info').fadeIn('slow', function() {
          done.wp_switch = true;
          done_cb();
        });
      });

    }

    if (window.ACC_ACTIVATE_SWITCH || window.ACC_ACTIVATE_FLOATING_BUTTON) {
      acc_webpush_elements.push(acc_webpush_arrow);
    }





    return defer.promise();
  }










  /**
   * Définit l'état de tous les éléments du wepbush.
   *
   */
  function set_global_state(state) {

    if (state === 'optin') {
      acc_webpush_elements.forEach(function(element) {
        window.setTimeout(function() { element.optin.call(element); }, 0);
      });

    } else if (state === 'optout') {
      acc_webpush_elements.forEach(function(element) {
        window.setTimeout(function() { element.optout.call(element); }, 0);
      });

    } else {
      acc_webpush_elements.forEach(function(element) {
        window.setTimeout(function() { element.unknown.call(element); }, 0);
      });

    }
  }





/**
 * Promisify jQuery.ready
 *
 */
function wait_for_dom() {
  var defer = $.Deferred();

  if (get_fake_optin_fn() === null) { store_fake_optin_fn(true); }

  $(function() { window.setTimeout(function() { defer.resolve(); }, 0); });

  return defer.promise();
}







/**
 * Début de l'éxécution
 *
 */
  wait_for_dom() // On attend le SDK

    .then(function() { return create_elements(); }) // On crée les éléments demandés par les shortcodes et on les attache au DOM
    //.then(function() { return wait_for_SDK(); }) // on attend le SDK
  	.then(function() { return check_compliance(); }) // On vérifie que le navigateur est compatible
    //.then(function() { return wait_for_dom(); }) // on attend le dom
    .then(function() {

  		return is_optin_sys_fn() // On vérifie l'optinisation de l'utilisateur
  		.then(function(result_state) {
        set_global_state(result_state); // On met tous les éléments au bon état
  		})
  		.fail(function(err) {
  			var defer = $.Deferred();
  			setTimeout(function() { return defer.reject(err); }, 0);
  			return defer.promise();
  		});



  	})
  	.fail(function() {
  		acc_current_state = 'not_compliant'; // Si une erreur est survenue on considère que le navigateur n'est pas compatible.
  	});



})();

})();
